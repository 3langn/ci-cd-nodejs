const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../index");
chai.should();
chai.use(chaiHttp);

describe("Hello", () => {
  it("should connect to the Server", async () => {
    const res = await chai.request(server).get("/");
    chai.expect(res).to.have.status(200);
    chai.expect(res.body.message).to.be.a("string");
  });
});
